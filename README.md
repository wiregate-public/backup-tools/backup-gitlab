# Gitlab backup with restic

## Requirements

1. Linux operating system with Ansible installed
2. An active Google Cloud Platform (GCP) account with appropriate permissions

## Overview

This repository contains scripts and configurations for backing up files from `self-managed Gitlab server` to an `Google Drive` using `restic`.

There are 2 options for creating a `Google Cloud service account`: 
 
 - manually with the GCP web interface
 - using ansible code

In any case, you need to have a Google Cloud account with the appropriate permissions. In addition, in case you are going to create a service account using ansible code, on the PC where the terraform and ansible code will be running, `gcloud cli` must be installed with the selected active account. Please use the command: `gcloud auth login`. If you create a service account and its key manually, you have to place the account JSON file here:`<yor_local_home_dir>/.gcp/<your_sa_credential_file>.json`.


## Ansible playbooks
You can find more information on how to install Ansible [here](https://docs.ansible.com/ansible/latest/installation_guide/index.html)

#### Overview

This Ansible playbook is used to set up backup for Gitlab server:

- `ansible/main.yaml`

The `main.yaml` playbook consists of two roles.

##### Ansible Roles:
1. `gcp-sa`: to create a Google service account and its key (if you already have a service account and its key, just comment out this role).
2. `restic-backup`: to install and configure restic and rclone


#### Steps to Deploy

1. Fill in all the variables in file `ansible/group_vars/all/vault.yaml`
    - localhost_user_profile: `/home/user`                                       ## user profile on localhost 
    - remote_user_profile: `/root`                                               ## user profile on gitlab server
    - gcp_project: `Your_GCP_Project_Id`
    - gcp_service_account_name: `gcp-backup-sa`
    - gcp_service_account_email: `{{ gcp_service_account_name }}@{{ gcp_project }}.iam.gserviceaccount.com`   # service account email for Id   
    - local_gcp_service_account_key: `{{ localhost_user_profile }}/.gcp/gcp_backup_sa.json`                   # place for service account credentials on localhost
    - restic_password: `Your_RESTIC_Password`                                    
    - restic_repo: `rclone:google_drive:gitlab_backup`
    - backup_script_path: `/usr/local/bin/gitlab_backup.sh`
    - rclone_config_path: `{{ remote_user_profile }}/.config/rclone` 
    - folder_id: `Your_backup_folder_id_on_google_drive`                         ##   Your google drive's folder id

2. Create a folder on your Google Drive and give `editor` rights to the service account we are going to create. His name is indicated in the gcp_service_account_name variable, you need to specify his email: i.e. your_gcp_sa_name@your-gcp-project.iam.gserviceaccount.com
3. Fill in the variable `folder_id` in file `ansible/group_vars/all/vault.yaml` with your just created folder_id 
4. cd project_root/ansible
5. `ansible-playbook main.yaml -vv`
6. Check the crontab schedule on the gitlab server: `crontab -e`
